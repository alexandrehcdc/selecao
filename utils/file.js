'use strict'

const {writeFile} = require('fs')
const LINE_REGEX = /[^\n]+/g
const DIGITS_REGEX = /[0-9]{2}/g

function getArrayOfLines (string) {
    if (typeof string !== "string") throw new TypeError("Received parameter is not a string")                
    return string.match(LINE_REGEX)
}

function getTimePosition (string) {
    if (typeof string !== "string") throw new TypeError("Received parameter is not a string")                        
    const digits = string.match(DIGITS_REGEX)
    return digits !== null ? digits : ['5']
}

function getJson (lecture, time) {
    if(!lecture || !time) throw new TypeError("Received parameter is not supposed to be null") 
    return {lecture: lecture, duration: time}
}

function getPositionOfHighestDuration (array, timeRemaining) {
    if(!array || !timeRemaining) throw new TypeError("Received parameter is not supposed to be null")         
    let highestValue = 0
    let lecture, indexOfArray
    array.map((object, index) => {
        if(parseInt(object.duration) >= highestValue && parseInt(object.duration) <= timeRemaining) {
            highestValue = array[index].duration
            lecture = array[index].lecture
            indexOfArray = index
        }
    })
    if (highestValue === 0) {
        return {}
    } else {
        return {highestValue, lecture, indexOfArray}
    }
}

function writeTracks (path, trackA, trackB) {
    const data = {trackA: trackA, trackB: trackB}
    writeFile(path, JSON.stringify(data), 'utf8', (err) => {
        if (err) throw err
        console.log(data)
    })
}

function shuffleArray (array) {
    if (!array) throw new TypeError("Received parameter is not supposed to be null")
    array.map((element, index) => {
        let generatedNumber = Math.floor(Math.random() * array.length)
        let helper = element
        array[index] = array[generatedNumber]
        array[generatedNumber] = helper
    })
    return array
}

module.exports = {
    getArrayOfLines, 
    getTimePosition, 
    getJson, 
    getPositionOfHighestDuration, 
    writeTracks,
    shuffleArray
}