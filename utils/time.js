'use strict'

function separateHoursFromMinutes (hours) {
    if (typeof hours !== "string") throw new TypeError("Received parameter is not a string")
    return hours.split(":")
}

function hoursToMinutes (hours) {
    if (typeof hours !== "string") throw new TypeError("Received parameter is not a string")        
    const splitHours = separateHoursFromMinutes(hours)
    return (parseInt(splitHours[0]) * 60) + parseInt(splitHours[1])
}

function minutesToHours (minutes) {
    if (typeof minutes !== "string" && typeof minutes !== "number") {
        throw new TypeError("Received parameter is not a string or a number")
    }
    const divMinutes = parseInt(minutes) / 60
    let modMinutes = parseInt(minutes) % 60
    if (modMinutes.toString().length === 1 || modMinutes === 0) {
        modMinutes = `0${modMinutes}`
    }
    return `${Math.floor(divMinutes)}:${modMinutes}`
}

function addMinutesToTimeSchedule (hours, minutes) {
    if (typeof minutes !== "string" && typeof minutes !== "number") {
        throw new TypeError("Received parameter is not a string or a number")
    }
    let hoursInMinutes = hoursToMinutes(hours)
    let sum = parseInt(hoursInMinutes) + parseInt(minutes)
    return minutesToHours(sum.toString())
}

function subtractMinutesFromTimeSchedule (hours, minutes) {
    if (typeof minutes !== "string" && typeof minutes !== "number") {
        throw new TypeError("Received parameter is not a string or a number")
    }
    let hoursInMinutes = hoursToMinutes(hours)
    let sum = parseInt(hoursInMinutes) - parseInt(minutes)
    return minutesToHours(sum.toString())
}
//repetido, refatorar**

module.exports =  {
    separateHoursFromMinutes, 
    hoursToMinutes, 
    minutesToHours, 
    addMinutesToTimeSchedule, 
    subtractMinutesFromTimeSchedule
}