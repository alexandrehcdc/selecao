'use strict'

const {timeUtils, fileUtils} = require('./utils')

function lectureModel (initialTime, lecture, endingTime) {
    return {initialTime: initialTime, lecture: lecture, endingTime: endingTime}
}

function endingTime (lastEndingTime, lectureTime) {
    const endingTime = timeUtils.hoursToMinutes(lastEndingTime)
    return timeUtils.minutesToHours(endingTime + parseInt(lectureTime))
}

function addLecturesToSchedule (lectureJson, schedule, timeShift, initialTime, callback) {
    let isThereTimeRemaining = true
    while (isThereTimeRemaining) {
        if (timeShift === 0 || lectureJson.length === 0) {
            isThereTimeRemaining = false
        } else {
            const lectureToAdd = fileUtils.getPositionOfHighestDuration(lectureJson, timeShift)
            schedule.push(`${initialTime} ${lectureJson[lectureToAdd.indexOfArray].lecture}`)
            lectureJson.splice(lectureToAdd.indexOfArray, 1)
            timeShift = timeShift - lectureToAdd.highestValue
            initialTime = timeUtils.addMinutesToTimeSchedule(initialTime, lectureToAdd.highestValue)
        }
    }
    callback(lectureJson, timeShift)
    return schedule
}

function mountTrackSchedule (json, schedule) {
    let initialShiftTime = '9:00'
    let afternoonShiftTime = '13:00'
    let lunchTimeInMinutes = 180
    let afternoonTimeInMinutes = 240
    schedule = addLecturesToSchedule(json, schedule, lunchTimeInMinutes, initialShiftTime ,(leftoverList, timeShift) => {
        lunchTimeInMinutes = timeShift
        json = leftoverList
        let activityTime = timeUtils.subtractMinutesFromTimeSchedule("12:00", timeShift)
        schedule.push(`${activityTime} Almoço`)
    })
    schedule = addLecturesToSchedule(json, schedule, afternoonTimeInMinutes, afternoonShiftTime, (leftoverList, timeShift) => {
        afternoonTimeInMinutes = timeShift
        json = leftoverList
        let activityTime = timeUtils.subtractMinutesFromTimeSchedule("17:00", timeShift)        
        schedule.push(`${activityTime} Networking`)
    })
    return schedule
}

module.exports = {addLecturesToSchedule, mountTrackSchedule}