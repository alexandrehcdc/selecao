'use strict'

const {readFile} = require('fs')
const {fileUtils} = require('./utils')
const trackMethods = require('./track')
const DIRECTORY_PATH = __dirname + '/proposals.txt'
const FILE_PATH = __dirname + '/tracks.json'

readFile(DIRECTORY_PATH, 'utf8', (err, data) => {
    let lunchTimeInMinutes = 180
    let afternoonTimeInMinutes = 240
    if (err) throw err
    let arrayOfLines = fileUtils.getArrayOfLines(data)
    let lectureList = [],
        trackA = [],
        trackB = []

    arrayOfLines.map(lecture => {
        lectureList.push(fileUtils.getJson(lecture, fileUtils.getTimePosition(lecture)[0]))
    })

    trackA = trackMethods.mountTrackSchedule(lectureList, trackA)
    trackB = trackMethods.mountTrackSchedule(lectureList, trackB)

    fileUtils.writeTracks(FILE_PATH, trackA, trackB)
})