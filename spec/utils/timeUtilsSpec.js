'use strict'

const {timeUtils} = require('../../utils')

describe("Utils tests", () => {

    describe("Time utils tests", () => {

        describe("separateHoursFromMinutes method", () => {
            it("Should throw an error if the argument is not an string", () => {
                expect(() => {
                    timeUtils.separateHoursFromMinutes(10)
                }).toThrowError("Received parameter is not a string")
            })
            it("Should split segments of the string (hours) that contains ':'", () => {
                let result = timeUtils.separateHoursFromMinutes("12:42")
                expect(result).toEqual(["12", "42"])
            })
            it("Should return equal string in case that the string itself doesn't contain ':'", () => {
                let result = timeUtils.separateHoursFromMinutes("1337")
                expect(result).toEqual(["1337"])                    
            })
        })

        describe("hoursToMinutes method", () => {
            it("Should throw an error if the argument is not an string", () => {
                expect(() => {
                    timeUtils.hoursToMinutes(10)
                }).toThrowError("Received parameter is not a string")
            })
            it("Should return the hours converted to minutes, as an integer", () => {
                let result = timeUtils.hoursToMinutes("3:00")
                expect(result).toEqual(180)
            })
            it("Should return the correct amount of minutes even if less than 1 hour is passed as parameter", () => {
                let result = timeUtils.hoursToMinutes("0:30")
                expect(result).toEqual(30)
            })
        })

        describe("minutesToHours method", () => {
            it("Should throw an error if the argument is not an string or a number", () => {
                expect(() => {
                    timeUtils.minutesToHours(true)
                }).toThrowError("Received parameter is not a string or a number")
            })
            it("Should return the minutes (number) converted to hours, as an string", () => {
                let result = timeUtils.minutesToHours(120)
                expect(result).toEqual("2:00")
            })
            it("Should return the minutes (string) converted to hours, as an string", () => {
                let result = timeUtils.minutesToHours("120")
                expect(result).toEqual("2:00")
            })
            it("Should return the correct amount of hours even if less than 1 hour (in minutes) is passed", () => {
                let result = timeUtils.minutesToHours("30")
                expect(result).toEqual("0:30")
            })
        })

        describe("addMinutesToTimeSchedule method", () => {
            it("Should throw an error if the first parameter is not an string", () => {
                expect(() => {
                    timeUtils.addMinutesToTimeSchedule(false, "100")
                }).toThrowError("Received parameter is not a string")
            })
            it("Should throw an error if the second parameter is not an string or a number", () => {
                expect(() => {
                    timeUtils.addMinutesToTimeSchedule("1:00", true)
                }).toThrowError("Received parameter is not a string or a number")
            })
            it("Should return a sum of the hours and minutes (string) passed as parameters", () => {
                let result = timeUtils.addMinutesToTimeSchedule("13:00", "37")
                expect(result).toEqual("13:37")
            })
            it("Should return a sum of the hours and minutes (number) passed as parameters", () => {
                let result = timeUtils.addMinutesToTimeSchedule("13:00", 37)
                expect(result).toEqual("13:37")
            })
        })

        describe("subtractMinutesFromTimeSchedule method", () => {
            it("Should throw an error if the first parameter is not an string", () => {
                expect(() => {
                    timeUtils.subtractMinutesFromTimeSchedule(false, "100")
                }).toThrowError("Received parameter is not a string")
            })
            it("Should throw an error if the second parameter is not an string or a number", () => {
                expect(() => {
                    timeUtils.subtractMinutesFromTimeSchedule("1:00", true)
                }).toThrowError("Received parameter is not a string or a number")
            })
            it("Should return a subtraction of the hours and minutes (string) passed as parameters", () => {
                let result = timeUtils.subtractMinutesFromTimeSchedule("14:00", "23")
                expect(result).toEqual("13:37")
            })
            it("Should return a subtraction of the hours and minutes (number) passed as parameters", () => {
                let result = timeUtils.subtractMinutesFromTimeSchedule("14:00", 23)
                expect(result).toEqual("13:37")
            })
        })
    })
})