'use strict'

const {fileUtils} = require('../../utils')
let mockLecture = ' Ruby vs. Clojure para desenvolvimento backend 30min'
let mockSchedule = [ ' Programação em par 45min ',
  ' Reinventando a roda em ASP clássico 45min',
  ' Ruby vs. Clojure para desenvolvimento backend 30min' ]
let mockScheduleWithoutLines = [ '10:30 Programação em par 45min 11:15 Reinventando a roda em ASP clássico 45min 12:00 Almoço 13:00 Ruby vs. Clojure para desenvolvimento backend 30min' ]
let mockText = " Programação em par 45min \n Reinventando a roda em ASP clássico 45min\n Ruby vs. Clojure para desenvolvimento backend 30min"
let mockTextWithoutLines = "10:30 Programação em par 45min 11:15 Reinventando a roda em ASP clássico 45min 12:00 Almoço 13:00 Ruby vs. Clojure para desenvolvimento backend 30min"

describe("Utils tests", () => {

    describe("File utils tests", () => {

        describe("getArrayOfLines method", () => {
            it("Should throw an error if the argument is not an string", () => {
                expect(() => {
                    fileUtils.getArrayOfLines(null)
                }).toThrowError("Received parameter is not a string")
            })
            
            it("Should return an array of strings, split by line separation", () => {
                let result = fileUtils.getArrayOfLines(mockText)
                expect(result).toEqual(mockSchedule)
            })
            
            it("Should return the same string in a array, in effect of not having linebreaking points", () => {
                let result = fileUtils.getArrayOfLines(mockTextWithoutLines)
                expect(result).toEqual(mockScheduleWithoutLines)
            })
        })

        describe("getTimePosition method", () => {
            it("Should throw an error if the argument is not an string", () => {
                expect(() => {
                    fileUtils.getTimePosition(null)
                }).toThrowError("Received parameter is not a string")
            })
            it("Should return the time related to the string position", () => {
                let result = fileUtils.getTimePosition(mockLecture)
                expect(result).toEqual(["30"])
            })
            it("Should return the time as 5 minutes if there is not a explicit time stamp on the string", () => {
                let result = fileUtils.getTimePosition('açougueiro vitaminado')
                expect(result).toEqual(["5"])
            })
        })

        describe("getJson method", () => {
            it("Should throw an error if any parameter is null", () => {
                expect(() => {
                    fileUtils.getJson(null, null)
                }).toThrowError("Received parameter is not supposed to be null")
            })
            it("Should return an JSON Object in case a lecture and time is passed as parameters", () => {
                let result = fileUtils.getJson(mockLecture, "30")
                expect(result).toEqual({ lecture: ' Ruby vs. Clojure para desenvolvimento backend 30min',
                duration: '30' })
            })
        })

        describe("getPositionOfHighestDuration method", () => {
            it("Should throw an error if any parameter is null", () => {
                expect(() => {
                    fileUtils.getPositionOfHighestDuration(null, null)
                }).toThrowError("Received parameter is not supposed to be null")
            })

            it("Should return the highestValue, lecture, and index of the array", () => {
                let json = []
                mockSchedule.map(lecture => {
                    json.push(fileUtils.getJson(lecture, fileUtils.getTimePosition(lecture)))
                })
                let result = fileUtils.getPositionOfHighestDuration(json, 30)
                expect(result).toEqual(
                    { highestValue: [ '30' ],
                    lecture: ' Ruby vs. Clojure para desenvolvimento backend 30min',
                    indexOfArray: 2 })
            })
        })

        describe("shuffleArray method", () => {
            it("Should throw an error if any parameter is null", () => {
                expect(() => {
                    fileUtils.shuffleArray(null)
                }).toThrowError("Received parameter is not supposed to be null")
            })

            it("Should shuffle and change array positions", () => {
                let result = mockSchedule
                fileUtils.shuffleArray(mockSchedule)
                expect(result).toEqual(mockSchedule)
            })
        })
    })
})